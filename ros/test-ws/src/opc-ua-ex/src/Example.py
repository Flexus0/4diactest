#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from ros_opcua_srvs.srv import *
from ros_opcua_msgs.msg import *

def connect():
    rospy.wait_for_service('/opcua/opcua_client/connect')
    try:
        connecto = rospy.ServiceProxy('/opcua/opcua_client/connect', Connect)
        resp1 = connecto("opc.tcp://localhost:4840")
        return resp1.success
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def subscribe():
    rospy.wait_for_service('/opcua/opcua_client/subscribe')
    try:
        subs = rospy.ServiceProxy('/opcua/opcua_client/subscribe', Subscribe)
	a = Address()
	a.qualifiedName = 'FlipROS'
	a.nodeId = 'ns=1;i=123'
        resp1 = subs(a, 'FlipTopic')
        return resp1.success
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def write(data):
    rospy.wait_for_service('/opcua/opcua_client/write')
    try:
        writed = rospy.ServiceProxy('/opcua/opcua_client/write', Write)
	a = Address()
	a.qualifiedName = 'FlopROS'
	a.nodeId = 'ns=1;i=1234'
	t = TypeValue()
	t.type = "bool"
	t.bool_d = not(data)
        resp1 = writed(a, t)
        return resp1.success
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def callback(data):
    print("callback")
    write(data.bool_d)

def usage():
    return "%s [x y]"%sys.argv[0]

if __name__ == "__main__":
    rospy.init_node('listener', anonymous=True)
    res = connect()
    print("OK")
    print(res)
    res2 = subscribe()
    print("OK2")
    print(res2)
    rospy.Subscriber("/opcua/opcua_client/FlipTopic", TypeValue, callback)
    rospy.spin()
